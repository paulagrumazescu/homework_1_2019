function distance(first, second){
	//TODO: implementați funcția
	// TODO: implement the function

	if (!Array.isArray(first) || !Array.isArray(second)) {
		console.log("Is first array?: " + Array.isArray(first));
		console.log("Is second array?: " + Array.isArray(second));
		throw new Error("InvalidType");
	}

	if (first.length == 0 && second.length == 0) {
		return 0;
	}

	let arr = first.concat(second);
	let uniqueArr = [...new Set(arr)];

	console.log(uniqueArr);

	let uniqueDataTypes = new Set();
	uniqueArr.forEach(element => uniqueDataTypes.add(typeof element));
	if (uniqueDataTypes.size > 1) {
		return uniqueDataTypes.size;
	}

	return uniqueArr.length - 1;
}


module.exports.distance = distance